﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConcursoFACCI
{
    class Concurso : IMostrarDatos
    {
        List<string> ConcursosRegistrados = new List<string>(); // Lista que almacenará los concursos
        public string NombreConcurso { get; set; }
        public int MConcurso { get; set; }
        public String EleccionCategoria { get; set; }
        public Concurso()
        {
            ConcursosRegistrados.Add("Lógica Básica");
            ConcursosRegistrados.Add("Lógica Avanzada");
            ConcursosRegistrados.Add("Empresarial");
            ConcursosRegistrados.Add("Retos");
        }


        public void MenuConcurso()
        {
            int validarUsuario;
            string opciónUsuario;

            do { 

                do { 
                    Console.Clear();
                    Console.WriteLine("▬▬▬▬▬▬▬▬▬▬ Concurso FACCI 2021 ▬▬▬▬▬▬▬▬▬▬");
                    Console.WriteLine("1. Registrar Concurso.\n2. Mostrar Concursos. \n3. Eliminar Concursos. \n4. Salir de este menú. \n5. Cerrar Programa.");
                    Console.Write("----------------------------\nIngrese una opción: ");
                    opciónUsuario = Console.ReadLine();

                    int.TryParse(opciónUsuario, out validarUsuario);

                    if (validarUsuario <= 0 || validarUsuario > 5)
                    {
                        Console.WriteLine("Ingrese una opción correcta. Pulse para continuar...");
                        Console.ReadKey();
                    }

                } while (validarUsuario <= 0 || validarUsuario > 5) ;

                    switch (validarUsuario)
                    {
                        case 1:
                            Console.Clear();
                            RegistrarConcurso();
                            break;
                        case 2:
                            Console.Clear();
                            MostrarConcursos();
                            break;
                        case 3:
                            Console.Clear();
                            EliminarConcurso();
                            break;
                        case 4:
                            Console.Clear();
                            if(ConcursosRegistrados.Count == 0)
                            {
                            Console.WriteLine("Primero registre al menos un concurso. Pulse para volver...");
                            Console.ReadKey();
                            }

                            break;
                        case 5:
                            Console.WriteLine("Gracias por usar el programa.");
                            System.Environment.Exit(5);
                            break;
                    }

            } while (validarUsuario != 4 && validarUsuario != 5 || ConcursosRegistrados.Count == 0);
        }


        public void RegistrarConcurso()
        {
            Console.WriteLine("Ingrese el nombre del concurso: ");
            this.NombreConcurso = Console.ReadLine();

            ConcursosRegistrados.Add(NombreConcurso);
            Console.WriteLine("Concurso agrgado con éxito. Presione para continuar...");
            Console.ReadKey();
        }

        public void MostrarConcursos()
        {
            Console.WriteLine("▬▬▬▬▬▬ Lista de Concursos ▬▬▬▬▬▬");

            if (ConcursosRegistrados.Count == 0)
            {
                Console.WriteLine("Aún no hay concursos registrados.");
            }

            else {

                Console.WriteLine("-------------------------------");

                foreach (string item in ConcursosRegistrados)
                {
                    Console.WriteLine("▬ " + item);
                    Console.WriteLine("-------------------------------");
                }

            }

            Console.WriteLine("Presione para continuar...");
            Console.Write("");
            Console.ReadKey();
        }

        public void EliminarConcurso()
        {
            string eliminarElemento = "";
            int validarElemento;

            do
            {               
                do
                {
                    Console.WriteLine("-----------------------");
                    Console.WriteLine("1. Ingresar nombre. \n2. Ver Lista\n3. Volver");
                    Console.Write("-----------------------\nIngrese la opción: ");
                    eliminarElemento = Console.ReadLine();

                    int.TryParse(eliminarElemento, out validarElemento);

                    if (validarElemento <= 0 || validarElemento > 3)
                    {
                        Console.Write("Ingrese una opción correcta. Pulse para continuar...");
                        Console.ReadKey();
                    }

                } while (validarElemento <= 0 || validarElemento > 3);

                switch (validarElemento)
                {
                    case 1:

                        Console.Clear();
                        string ElementoEliminar;
                        int controlEliminar, controlEliminarF;
                        Console.Write("Ingrese el nombre del elemento: ");
                        controlEliminarF = ConcursosRegistrados.Count;
                        ElementoEliminar = Console.ReadLine();

                        ConcursosRegistrados.Remove(ElementoEliminar);
                        controlEliminar = ConcursosRegistrados.Count;

                            if (controlEliminar != controlEliminarF)
                            {
                                Console.Write("El concurso se eliminó con éxito. Pulse para continuar...");
                                Console.ReadKey();
                            }
                            else
                            {
                                Console.WriteLine("El concurso no existe");
                            }

                        break;

                    case 2:
                        Console.Clear();
                        MostrarConcursos();
                        break;
                }

            } while (validarElemento != 3);                       

        }

        public string ElegirConcurso()
        {
            Console.Clear();
            
            int validar = 0;

            MostrarConcursos();
            Console.WriteLine("----------------------------------------------");
            do
            {
                Console.Write("Ingrese el nombre de la categoría a elegir: ");
                this.EleccionCategoria = Console.ReadLine();

                if(ConcursosRegistrados.Contains(EleccionCategoria))
                {
                    validar = 1;
                    this.MConcurso = ConcursosRegistrados.IndexOf(EleccionCategoria);
                    Console.WriteLine("Categoria elegida con éxito. Pulse para continuar");
                    Console.ReadKey();
                    
                }
                else
                {
                    Console.WriteLine("El concurso no existe. Ingrese uno correcto");
                }
               
                

            } while (validar != 1);

            return NombreConcurso;
        }

        public void ImprimirDatos()
        {
            Console.WriteLine("El último concursante registrado ha elegido la categoria " + EleccionCategoria);
            Console.WriteLine("-------------------------------------------------");
        }


    }
}
