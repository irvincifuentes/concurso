﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConcursoFACCI
{
    class Computadora : IMostrarDatos
    {
        List<int> NumeroComputadura = new List<int>()
        {
             1, 2, 3, 4, 5, 6, 7, 8, 9, 10
        };

        List<string> IDEComputadura = new List<string>();

        public string IDE { get; set; }
        public int NComputadora { get; set; }
        public Computadora()
        {
            IDEComputadura.Add("Visual Studio");
            IDEComputadura.Add("PostgreSQL");
            IDEComputadura.Add("SQL Server");
        }

        public void MenuComputadoras()
        {
            string validar;
            int opcionUsuario;

            Console.Clear();
            Console.WriteLine("El software que viene en la computadora son los siguientes: \n");
            foreach (string item in IDEComputadura)
            {
                Console.WriteLine("▬ "+ item);

            }
            Console.WriteLine("-------------------------------");

            do
            {

            
                Console.WriteLine("\n¿Desea añadir otro software?\n1. Sí \n2. No");
                Console.Write("Ingrese su opción: ");
                validar = Console.ReadLine();

                int.TryParse(validar, out opcionUsuario);

                if (opcionUsuario <= 0 || opcionUsuario > 2)
                {
                    Console.WriteLine("Ingrese una opción correcta. Pulse para volver a intentar...");
                    Console.ReadKey();
                }

                switch (opcionUsuario)
                {
                    case 1:
                        Console.Clear();
                        AniadirIDE();
                        break;
                    case 2:
                        Console.WriteLine("\nUsted no registrará más IDE. Pulse para continuar...");
                        Console.ReadKey();
                        break;
                }

            } while (opcionUsuario != 2);

        }

        public void AniadirIDE()
        {
            Console.WriteLine("Ingrese el nombre del IDE: ");
            this.IDE = Console.ReadLine();

            IDEComputadura.Add(IDE);
            Console.WriteLine("\nIDE agrgado con éxito. Presione para continuar...");
            Console.ReadKey();
        }

        public void ElegirComputadora()
        {
            Console.Clear();

            int validar = 0;

            Console.WriteLine("▬▬▬▬▬ Computadoras Disponibles ▬▬▬▬▬");
            foreach (int item in NumeroComputadura)
            {
                Console.WriteLine("Computadora #" + item);
            }
            Console.WriteLine("----------------------------------------------");
            do
            {
                Console.Write("Ingrese el número de la computadora a elegir: ");
                this.NComputadora = int.Parse(Console.ReadLine());

                if (NumeroComputadura.Contains(NComputadora))
                {
                    validar = 1;
                    Console.WriteLine("Computadora elegida con éxito. Pulse para continuar");
                    Console.ReadKey();
                }
                else
                {
                    
                    Console.WriteLine("Solo hay 10 computadoras. Ingrese una correcto");
                }



            } while (validar != 1);
        }

        public void ImprimirDatos()
        {
            int Comparar = IDEComputadura.Count;

            Console.WriteLine("Usted ha elegido la " + NComputadora + " Computadora");
            NumeroComputadura.Remove(NComputadora);
            Console.WriteLine("-------------------------------------------------");

            if (this.IDE != null)
            {
                Console.WriteLine("Los IDE escogidos son: \n");

                foreach (string item in IDEComputadura)
                {
                    Console.WriteLine(item);
                }

                Console.WriteLine("\nUsted ha añadido el IDE " + IDE);
            }
            else
            {
                Console.WriteLine("Usted solo ha elegido los IDE predeterminados: ");
                foreach (string item in IDEComputadura)
                {
                    Console.WriteLine(item);
                }
            }

            Console.WriteLine("-------------------------------------------------");
        }

    }
}
