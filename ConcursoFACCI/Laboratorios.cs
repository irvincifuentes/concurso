﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConcursoFACCI
{
    class Laboratorios : IMostrarDatos
    {
        public string ProgramasGenerales { get; set; }
        public Laboratorios()
        {
           this.ProgramasGenerales = "\n1. Zoom" + "\n2. Teams" + "\n3. Paquete Oficce" + "\n4. Navegador Web";

        }

        public void ImprimirDatos()
        {
            Console.WriteLine("Los programas predefinidos en todos los laboratorios son: " +this.ProgramasGenerales);
            Console.WriteLine("-------------------------------------------------");
        }

    }

    
}
