﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConcursoFACCI
{
    class Usuario : IMostrarDatos
    {
        public string ApellidosNombres { get; set; }
        List<string> Concursantes = new List<string>();


        public Usuario()
        {

        }

        public void AniadirConcursante()
        {
            Console.Write("Ingrese los apellidos y nombres del concursante a inscribir: ");
            this.ApellidosNombres = Console.ReadLine();
            Concursantes.Add(ApellidosNombres);

        }

        public void ImprimirDatos()
        {
            if(this.ApellidosNombres == null)
            {
                Console.WriteLine("▬▬▬▬▬ Concursantes Registrados ▬▬▬▬▬");
                Console.WriteLine("Sin concursantes registrados. ");
                AniadirConcursante();
            }

            Console.WriteLine("▬▬▬▬▬ Concursantes Registrados ▬▬▬▬▬");
            foreach (string item in Concursantes)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("-------------------------------------------------");
        }
    



    }
}
