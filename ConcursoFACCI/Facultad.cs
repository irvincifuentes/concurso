﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConcursoFACCI
{
    class Facultad : Concurso, IMostrarDatos
    {

        public int EleccionLaboratorio { get; set; }
        public string NombreFacultad { get; set; }

        Usuario usuario = null;

        List<string> Laboratorios = new List<string>()
            {
                "Laboratorio 1",
                "Laboratorio 2",
                "Laboratorio 3",

        };


        public Facultad(Usuario usuario) 
        {               
            this.NombreFacultad = "Ciencias Informáticas";
            Laboratorios NumeroLaboratorios = new Laboratorios();


        }

        public void SinConcursante(Usuario usuario) // Agregación, no puedo inscribir a nadie en un concurso sin que se registre antes.
        {
            
            string validar;
            int opcionUsuario;

            if(usuario != null) { 

                do { 
                    Console.Write("Aún no se ha agregado un usuario. ¿Desea añadir uno? \n1. Sí\n2. No\n----------------------");
                    Console.Write("\nIngrese su opción: ");
                    validar = Console.ReadLine();

                    int.TryParse(validar, out opcionUsuario);

                    if (opcionUsuario <= 0 || opcionUsuario > 2)
                    {
                        Console.WriteLine("Ingrese una opción correcta. Pulse para continuar...");
                        Console.ReadKey();
                    }

                    else
                    { 

                        switch (opcionUsuario)
                        {
                            case 1:
                                Console.Clear();
                                usuario.AniadirConcursante(); // Agregación
                                break;
                            case 2:
                                Console.Clear();
                                Console.WriteLine("Usted ha elegido no añadir concursantes. Pulse para continuar");
                                Console.ReadKey();
                                Console.Clear();
                                usuario.AniadirConcursante();
                                
                                break;
                        }

                    }

                } while (opcionUsuario <= 0 || opcionUsuario > 2);
            }

        }

        public void ImprimirDatos()
        {
            Concurso LaboratorioConcurso = new Concurso();
            foreach (string item in Laboratorios)
            {

            }

            if(LaboratorioConcurso.MConcurso == 0)
            {
                Console.WriteLine("La categoría elegida está asignada al " + Laboratorios[0]);
            }
            if (LaboratorioConcurso.MConcurso == 1)
            {
                Console.WriteLine("La categoría elegida está asignada al " + Laboratorios[1]);
            }
            if (LaboratorioConcurso.MConcurso == 2)
            {
                Console.WriteLine("La categoría elegida está asignada al " + Laboratorios[2]);
            }
            Console.WriteLine("-------------------------------------------------");
        }


    }
}
