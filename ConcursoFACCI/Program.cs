﻿using System;
using System.Collections.Generic;

namespace ConcursoFACCI
{
    class Program
    {
        static void Main(string[] args)
        {
            int opcionUsuario;
            string validar;

            Concurso concurso = new Concurso();
            Usuario usuario = new Usuario();
            Facultad facultad = new Facultad(usuario);
            Computadora computadora = new Computadora();
            Laboratorios laboratorios = new Laboratorios();
            concurso.MenuConcurso();

            do
            {
               
                facultad.SinConcursante(usuario);
                concurso.ElegirConcurso();
                computadora.ElegirComputadora();
                computadora.MenuComputadoras();
            
                do
                {
                    Console.Clear();
                    Console.WriteLine("Desea ver los datos registrados: \n1. Si\n2. No");
                    Console.Write("Ingrese su elección: ");
                    validar = Console.ReadLine();
                    int.TryParse(validar, out opcionUsuario);

                    if (opcionUsuario <= 0 || opcionUsuario > 2)
                    {
                        Console.WriteLine("Opción inválida, ingrese una correcta. Pulse para continuar...");
                        Console.ReadKey();
                    }

                } while (opcionUsuario != 1);

                    switch (opcionUsuario)
                    {
                        case 1:
                            usuario.ImprimirDatos();
                            concurso.ImprimirDatos();
                            facultad.ImprimirDatos();
                            laboratorios.ImprimirDatos();
                            computadora.ImprimirDatos();
                            Console.ReadKey();
                            break;

                        case 2:
                            Console.WriteLine("Desea añadir otro concursante: \n1. Sí\n2. No. (Salir del programa)");
                                do
                                {                            
                                    Console.Write("Ingrese su elección: ");
                                    validar = Console.ReadLine();
                                    int.TryParse(validar, out opcionUsuario);

                                    if (opcionUsuario <= 0 || opcionUsuario > 2)
                                    {
                                        Console.WriteLine("Opción inválida, ingrese una correcta. Pulse para continuar...");
                                        Console.ReadKey();
                                    }

                                } while (opcionUsuario <= 0 || opcionUsuario > 2);

                                switch (opcionUsuario)
                                {
                                    case 1:
                                    opcionUsuario = 1;
                                        break;
                                    case 2:
                                        System.Environment.Exit(2);
                                    break;
                                        }

                            break;
                    }

            } while (opcionUsuario != 2);


        }
    }
}
