﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Seguimiento_POO1
{
    class Profesor : Empleado
    {

        public string departamento_Profesor { get; set; }

        public void cambio_Departamento(string nuevo_Departamento)
        {
            this.departamento_Profesor = nuevo_Departamento;
        }

        public override string imprimir_Datos()
        {

            return "Nombres: " + this.nombre_Persona + "\nApellidos: " + this.apellido_Persona + "\nNúmero de identificación: " + this.identificacion_Persona + "\nEstado Civil: " + this.estadocivil_Persona + "\nDepartamento asignado: " + this.departamento_Profesor + "\nAño incorporación: " + this.anioIncorporacion_Empleado + "\nDespacho asignado: " + this.despacho_Empleado;
        }

    }
}
