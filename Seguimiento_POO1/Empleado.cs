﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Seguimiento_POO1
{
    class Empleado : Persona
    {

        public int anioIncorporacion_Empleado { get; set; }
        public int despacho_Empleado { get; set; }


        public void reasignar_Despacho(int nuevo_Despacho)
        {
            this.despacho_Empleado = nuevo_Despacho;
        }


        public override string imprimir_Datos() => "";

    }

}
