﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Seguimiento_POO1
{
    class Estudiante : Persona
    {
        
        public int curso_Estudiante { get; set; }


        public void matricula_NuevoCurso(int nuevo_Curso)
        {
            this.curso_Estudiante = nuevo_Curso;
        }


        public override string imprimir_Datos()
        {
            return "Nombres: " + this.nombre_Persona + "\nApellidos: " + this.apellido_Persona + "\nNúmero de identificación: " + this.identificacion_Persona + "\nEstado Civil: " + this.estadocivil_Persona + "\nSemestre: " + this.curso_Estudiante;
        }

    }
}
