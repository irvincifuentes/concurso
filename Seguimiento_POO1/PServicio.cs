﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Seguimiento_POO1
{
    class PServicio : Empleado
    {

        public string seccion_PServicio { get; set; }


        public void cambio_Seccion(string nueva_Seccion)
        {
            this.seccion_PServicio = nueva_Seccion;
        }

        public override string imprimir_Datos()
        {
            Empleado empleado = new Empleado();

                return "Nombres: " + this.nombre_Persona + "\nApellidos: " + this.apellido_Persona + "\nNúmero de identificación: " + this.identificacion_Persona + "\nEstado Civil: " + this.estadocivil_Persona + "\nSección asignada: " + this.seccion_PServicio + "\nAño de incorporación: " + this.anioIncorporacion_Empleado + "\nDespacho asignado: " + this.despacho_Empleado; ;
        }

    }
}
