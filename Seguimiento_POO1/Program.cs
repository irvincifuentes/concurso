﻿using System;

namespace Seguimiento_POO1
{
    class Program
    {
        static void Main(string[] args)
        {

            Estudiante estudiante = new Estudiante();
            Console.WriteLine("------- Estudiante -------");
            estudiante.nombre_Persona = "Irvin Gustavo";
            estudiante.apellido_Persona = "Cifuentes Castro";
            estudiante.estadocivil_Persona = "Soltero";
            estudiante.identificacion_Persona = "131346515-3";
            estudiante.matricula_NuevoCurso(3);
            Console.WriteLine(estudiante.imprimir_Datos());
            Console.WriteLine(" ");

            Profesor profesor = new Profesor();
            Console.WriteLine("------- Profesor -------");
            profesor.nombre_Persona = "Joffre Edgardo";
            profesor.apellido_Persona = "Panchana Flores";
            profesor.estadocivil_Persona = "Soltero";
            profesor.identificacion_Persona = "151594621-4";
            profesor.cambio_Departamento("POO");
            profesor.anioIncorporacion_Empleado = 1999;
            profesor.despacho_Empleado = 7;
            Console.WriteLine(profesor.imprimir_Datos());
            Console.WriteLine(" ");

            PServicio pServicio = new PServicio();
            Console.WriteLine("--- Personal de Servicio ---");
            pServicio.nombre_Persona = "Mario Alonso";
            pServicio.apellido_Persona = "Delgado Lopez";
            pServicio.estadocivil_Persona = "Soltero";
            pServicio.identificacion_Persona = "111512321-4";
            pServicio.cambio_Seccion("Biblioteca");
            pServicio.anioIncorporacion_Empleado = 2002;
            pServicio.despacho_Empleado = 47;
            Console.WriteLine(pServicio.imprimir_Datos());


        }
    }
}
