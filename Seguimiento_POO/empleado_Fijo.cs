﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Seguimiento_POO
{
    class empleado_Fijo : empleados_General
    {
        public DateTime FechaEntrada { get; set; }

        private decimal SueldoBase { get; set; }
        private decimal Complemento { get; set; }

        public empleado_Fijo()
        {
            this.SueldoBase = 400;
            this.Complemento = 2;
        }

        private int CalculoAniosEmpresa()
        {
            TimeSpan aniosEmpresa = DateTime.Today - this.FechaEntrada;
            DateTime tiempoTotal = new DateTime(aniosEmpresa.Ticks);
            return tiempoTotal.Year;

        }

        public decimal CalculoSueldo()
        {
            return this.SueldoBase + (this.Complemento * CalculoAniosEmpresa());
        }
    }
}
