﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Seguimiento_POO
{
    class empleado_Horas : empleados_General
    {
        private decimal PrecioHora { get; set; }//Valor Fijo
        public int NumeroHoras { get; set; }//Variable

        public empleado_Horas()
        {
            this.PrecioHora = 6;
        }

        public decimal CalcularSueldo()
        {   
            return this.PrecioHora * this.NumeroHoras;
        }
    }
}
